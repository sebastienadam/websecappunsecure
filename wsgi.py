import sys
from unsecured import create_app

sys.path.insert(0, '/var/www/unsecured')

application = create_app()