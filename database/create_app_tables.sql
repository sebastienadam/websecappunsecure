-- users table

CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

INSERT INTO users (username, password) VALUES ('admin', 'admin');
INSERT INTO users (username, password) VALUES ('user', 'password');

CREATE TABLE posts (
  id INTEGER PRIMARY KEY,
  author_id INTEGER REFERENCES users(id),
  created TEXT NOT NULL DEFAULT current_timestamp,
  title TEXT NOT NULL,
  body TEXT NOT NULL
);

CREATE TABLE comments (
  id INTEGER PRIMARY KEY,
  post_id INTEGER REFERENCES posts(id),
  created TEXT NOT NULL DEFAULT current_timestamp,
  author TEXT NOT NULL,
  body TEXT NOT NULL
);
