#!/usr/bin/bash

. ./venv/bin/activate
export FLASK_APP=unsecured
export FLASK_RUN_PORT=8080
flask --debug run
