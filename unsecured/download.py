from flask import Blueprint, current_app, render_template, request, send_file
from pathlib import Path

bp = Blueprint('download', __name__, url_prefix='/download')

@bp.route('/', methods=('GET', ))
def index():
    """
    List downloadable files (content of 'instance/download')
    """
    download_dir = f'{current_app.instance_path}/download'
    Path(download_dir).mkdir(parents=True, exist_ok=True)
    #print(download_dir)
    files = sorted(file_.name for file_ in Path(download_dir).glob('*'))
    return render_template('download/index.html', files=files)

@bp.route('/file', methods=('GET', ))
def download_file():
    """
    Download given filename
    """
    filename = request.args.get('filename')
    file_path = f'{current_app.instance_path}/download/{filename}'
    #print(file_path)
    return send_file(file_path, as_attachment=True)
