from flask import Blueprint, current_app, g, redirect, render_template, url_for
from .auth import login_required
from .models import Users
from .plugins import get_plugins_list

bp = Blueprint('admin', __name__, url_prefix='/admin')

@bp.route('/', methods=('GET', ))
@login_required
def dashboard():
    if g.user.username != 'admin':
        return redirect(url_for('index'))
    users = Users()
    users.load()
    plugins = get_plugins_list()
    return render_template('admin/dashboard.html', users=users.items, pp=plugins)
