import functools

from flask import Blueprint, flash, g, redirect, render_template, request, session, url_for
from .models import Auth

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.before_app_request
def load_logged_in_user():
    """
    Before each request check if a user is registered or not.
    """
    user_id = session.get('user_id')
    g.user = Auth()
    g.user.load_logged_user(user_id)

@bp.route('/login', methods=('GET', 'POST'))
def login():
    """
    If data are presents, authenticate user, and then return to index page.
    Otherwise, show login form.
    """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        g.user.login(username, password)
        if g.user.is_logged():
            session['user_id'] = g.user.id_
            return redirect(url_for('index'))
        flash('Unable to login.')
    return render_template('auth/login.html')

@bp.route('/logout', methods=('GET', ))
def logout():
    """
    Clear all authentication data.
    """
    session.clear()
    g.user.logout()
    return redirect(url_for('index'))

def login_required(view):
    """
    Function that can be used as decorator to force authentication.
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None or not g.user.is_logged():
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view
