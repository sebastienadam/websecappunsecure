from flask import Blueprint, flash, g, redirect, render_template, request, url_for
from .auth import login_required
from .models import Comment
from .models import Comments
from .models import Post
from .models import Posts

bp = Blueprint('blog', __name__, url_prefix='/blog')


@bp.route('/')
def index():
    """
    Show all posts (without comments)
    """
    posts = Posts()
    posts.load()
    return render_template('blog/index.html', posts=posts.items)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    """
    If data are present, create a new post and then redirect to posts list.
    If no data are present, show post creation form.
    """
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        errors = []

        if not title:
            errors.append('Title is required.')
        if not body:
            errors.append('Body is required.')

        if len(errors) > 0:
            flash(' -- '.join(errors))
        else:
            post = Post()
            post.title = title
            post.body = body
            post.author_id = g.user.id_
            post.save()
            return redirect(url_for('blog.index'))
    return render_template('blog/create.html')


@bp.route('/post', methods=('GET','POST'))
def show():
    """
    Show a single post, post ID has to be given
    """
    if request.method == 'POST':
        post_id = request.form['post_id']
        author = request.form['author']
        body = request.form['body']
        errors = []
        if not author:
            errors.append('Author is required.')
        if not body:
            errors.append('Comment is required.')
        if len(errors) > 0:
            flash(' -- '.join(errors))
        else:
            comment = Comment()
            comment.post_id = post_id
            comment.author = author
            comment.body = body
            comment.save()
    else:
        post_id = request.args.get('id')
    if post_id is None:
        return redirect(url_for('blog.index'))
    post = Post()
    post.load(post_id)
    comments = Comments()
    comments.load(post_id)
    return render_template('blog/post.html', post=post, comments=comments.items)


@bp.route('/update', methods=('GET', 'POST'))
def update():
    """
    Is it really necessary?
    """
    return render_template('common/under_construction.html')
