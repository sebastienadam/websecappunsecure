from flask import Blueprint, abort, current_app, render_template
from pathlib import Path
import importlib

bp = Blueprint('plugin', __name__, url_prefix='/plugin')

# Load plugins
def get_plugins_list():
    plugins = []
    for plugin in Path(f'{current_app.root_path}/plugins').glob('*.py'):
        if plugin.name == "__init__.py":
            continue
        plugins.append(plugin.stem)
    return plugins

@bp.route('/', methods=('GET', 'POST'))
def index():
    plugins = get_plugins_list()
    return render_template('plugins/index.html', pp=plugins)

@bp.route('/<path:plugin>', methods=('GET', 'POST'))
def plugin(plugin):
    plugins = get_plugins_list()
    if not plugin in plugins:
        abort(404)
    p = importlib.import_module(f'unsecured.plugins.{plugin}')
    return p.execute()
