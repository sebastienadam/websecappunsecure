from flask import Blueprint, current_app, render_template, request
from pathlib import Path

def execute():
    if request.method == 'POST':
        file_ = request.files['file']
        target = request.form['target']
        if target == 'plugin':
            target_path = Path(current_app.root_path) / 'plugins'
        elif target == 'plugin_template':
            target_path = Path(current_app.root_path) / 'templates' / 'plugins'
        else:
            target_path = Path(current_app.instance_path) / 'download'
        file_.save(target_path / file_.filename)
    return render_template('plugins/upload.html')
