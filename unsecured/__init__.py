from flask import Flask, render_template, request, send_file
from pathlib import Path

from . import admin
from . import auth
from . import blog
from . import db
from . import download
from . import errors
from . import plugins

def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # ensure the instance folder exists
    Path(app.instance_path).mkdir(parents=True, exist_ok=True)

    # Load config
    app.config.from_pyfile('app.cfg', silent=True)

    # Initialise db
    db.init_app(app)

    # Managing errors
    app.register_error_handler(404, errors.page_not_found)
    app.register_error_handler(500, errors.internal_server_error)

    # Load modules
    app.register_blueprint(admin.bp)
    app.register_blueprint(auth.bp)
    app.register_blueprint(blog.bp)
    app.register_blueprint(download.bp)
    app.register_blueprint(plugins.bp)

    # main page
    @app.route('/')
    def index():
        return render_template('common/index.html')

    # Static files
    @app.route('/<path:path>')
    def static_from_root(path):
        return send_file(f'{app.static_folder}/{path}')

    return app
