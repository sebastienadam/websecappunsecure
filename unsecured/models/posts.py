from datetime import datetime
from unsecured.db import get_db

class Posts():
    def __init__(self):
        self.items = []

    def load(self):
        db = get_db()
        query = 'SELECT posts.id, posts.author_id, users.username AS author, posts.created, posts.title, posts.body FROM posts JOIN users ON posts.author_id = users.id ORDER BY posts.created DESC;'
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            posts = cur.fetchall()
            for post in posts:
                #print(post)
                self.items.append({
                    'id_' : post[0],
                    'author_id' : post[1],
                    'author' : post[2],
                    'created' : post[3],
                    'title' : post[4],
                    'body' : post[5]
                })
