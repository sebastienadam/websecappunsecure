from unsecured.db import get_db

class Users():
    def __init__(self):
        self.items = []

    def load(self):
        self.items = []
        db = get_db()
        query = 'SELECT id, username FROM users ORDER BY username ASC;'
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            users = cur.fetchall()
            for user in users:
                #print(user)
                self.items.append({'id' : user[0], 'username' : user[1]})
