from datetime import datetime
from unsecured.db import get_db

class Comment():
    def __init__(self):
        self.clear()

    def clear(self):
        self.id_ = None
        self.post_id = None
        self.created = None
        self.author = None
        self.body = None

    def load(self, id_):
        self.clear()
        db = get_db()
        query = f'SELECT id, post_id, created, author, body FROM comments WHERE id = {id_};'
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            post = cur.fetchone()
            #print(post)
            self.id_ = post[0]
            self.post_id = post[1]
            self.created = post[2]
            self.author = post[3]
            self.body = post[4]

    def save(self):
        if self.id_ is None:
            query = f"INSERT INTO comments (post_id, author, body) VALUES ({self.post_id}, '{self.author}', '{self.body}')"
        else:
            query = f"UPDATE comments SET author = '{self.author}', body = '{self.body}' WHERE id = '{self.id_ }'"
        db = get_db()
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
