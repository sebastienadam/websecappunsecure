from unsecured.db import get_db

class Auth():
    def __init__(self):
        self.logout()

    def is_logged(self):
        return self.id_ is not None

    def load_logged_user(self, id_):
        if id_ is None:
            return
        self.id_ = id_
        db = get_db()
        cur = db.conn.cursor()
        with db.conn:
            query = f"SELECT username FROM users WHERE id = {self.id_};"
            cur.execute(query)
            self.username = cur.fetchone()[0]

    def login(self, username, password):
        db = get_db()
        query = f"SELECT id, username FROM users WHERE username = '{username}' AND password = '{password}';"
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            result = cur.fetchone()
            if result is None:
                self.logout()
            else:
                self.id_, self.username = result
            
    def logout(self):
        self.id_ = None
        self.username = None
