from datetime import datetime
from unsecured.db import get_db

class Comments():
    def __init__(self):
        self.items = []

    def load(self, post_id):
        self.items = []
        db = get_db()
        query = f'SELECT id, created, author, body FROM comments WHERE post_id = {post_id} ORDER BY created DESC;'
        with db.conn:
            cur = db.conn.cursor()
            try:
                cur.execute(query)
                comments = cur.fetchall()
                for comment in comments:
                    #print(post)
                    self.items.append({
                        'id_' : comment[0],
                        'created' : comment[1],
                        'author' : comment[2],
                        'body' : comment[3]
                    })
            except:
                pass
