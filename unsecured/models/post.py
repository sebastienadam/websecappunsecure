from datetime import datetime
from unsecured.db import get_db

class Post():
    def __init__(self):
        self.clear()

    def clear(self):
        self.id_ = None
        self.author_id = None
        self.author = None
        self.created = None
        self.title = None
        self.body = None

    def load(self, id_):
        self.clear()
        db = get_db()
        query = f'SELECT posts.id, posts.author_id, users.username AS author, posts.created, posts.title, posts.body FROM posts JOIN users ON posts.author_id = users.id WHERE posts.id = {id_};'
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            post = cur.fetchone()
            #print(post)
            self.id_ = post[0]
            self.author_id = post[1]
            self.author = post[2]
            self.created = post[3]
            self.title = post[4]
            self.body = post[5]

    def save(self):
        #print(self.id_)
        if self.id_ is None:
            query = f"INSERT INTO posts (author_id, title, body) VALUES ({self.author_id}, '{self.title}', '{self.body}')"
            #print('create new post')
        else:
            query = f"UPDATE posts SET title = '{self.title}', body = '{self.body}' WHERE id = '{self.id_}'"
            #print(f'update existing post (id {self.id_})')
        db = get_db()
        with db.conn:
            cur = db.conn.cursor()
            cur.execute(query)
            #db.conn.commit()
        #self.load(self.id_)
