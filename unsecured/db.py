from flask import current_app, g
#import psycopg
import sqlite3

class DB():
    """
    Manage DB connection
    """
    def __init__(self):
        self.conn = None
        self.connect()

    def connect(self):
        db_path = f'{current_app.instance_path}/{current_app.config["DATABASE_NAME"]}'
        #print(db_path)
        self.conn = sqlite3.connect(db_path)

    def close(self):
        self.conn.close()

def get_db():
    """
    Retrieve the DB connection if exists, otherwise create it
    """
    if 'db' not in g:
        g.db = DB()
    return g.db

def close_db(e=None):
    """
    Close connection to the DB
    """
    db = g.pop('db', None)
    if db is not None:
        db.close()


def init_app(app):
    """
    Configure app to close automatically connection to DB when stopping
    """
    app.teardown_appcontext(close_db)
