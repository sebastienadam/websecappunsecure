# WebSecAppUnsecure

Unsecured web application for web application security course.

## Preamble

The application must be installed in the `/var/www/unsecured` directory.

## Creating virtual environment

To create the Python Virtual Environment, proceed like this:

```
python3 -m venv venv
. venv/bin/activate
pip install -U pip
pip install wheel
pip install -U setuptools
pip install -r requirements.txt
```

## Installation

Create a `instance` folder with a `app.cfg` containing:

```
APP_NAME="Unsecured Web App"
DATABASE_NAME="unsecured.sqlite3"
SECRET_KEY="<this_is_my_secret_key_to_secure_cookies>"
SESSION_COOKIE_HTTPONLY=False
```

Then run this command (`sqlite3` has to be installed first) in the `instance` folder

```sh
sqlite3 unsecured.sqlite3 < ../database/create_app_tables.sql
```

Also create a `instance/download` folder with some files inside that can be downloaded.

## Generate self-signed certificate

Run following command filling all fields:

```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-unsecured.key -out /etc/ssl/certs/apache-unsecured.crt
```

## Configure Apache server

Install and activate `wsgi` module.

Create a `logs` directory into base directory.

Copy file `unsecured.conf` into `/etc/apache2/sites-available`.

Run commands:

```sh
sudo a2enmod ssl               # activate https
sudo a2ensite unsecured        # activate unsecured web site
sudo systemctl reload apache2  # restart apache sever
```

## Configure domain name

Add following line into `/etc/hosts` file:

```
127.0.2.1	unsecured.local unsecured
```

You can now access Unsecured web application through address https://unsecured.local/

-------

*&copy; Sébastien Adam 2022 ~ 2024*